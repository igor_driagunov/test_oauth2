import requests
import pytest
import json

from jsonschema import validate


@pytest.fixture(scope='module')
def load_env():
    env = json.load(open('./env/env.json'))
    users_url = env['users_url']
    auth_url = env['auth_url']
    username = env['username']
    password = env['password']
    grant_type = env['grant_type']

    return users_url, auth_url, username, password, grant_type


def test_oauth(load_env):
    users_url, auth_url, username, password, grant_type = load_env
    url = f'{auth_url}?username={username}&password={password}&grant_type={grant_type}'

    r = requests.post(
        url=url,
    )

    assert r.ok

    response = r.json()
    # print(response)

    assert 'access_token' in response and response['access_token'] is not None
    assert 'refresh_token' in response and response['refresh_token'] is not None

    schema_path = './schemas'
    validate(response, json.load(
        open(f'{schema_path}/oauth.json', encoding='utf8')
    )
             )

    return response['access_token']


def test_user_data(load_env):
    users_url, auth_url, username, password, grant_type = load_env
    url = users_url

    token = test_oauth(load_env)
    print(token)
    print()

    headers = {
        'Authorization': f'Bearer {token}'
    }

    r = requests.get(
        url=url,
        headers=headers
    )

    assert r.ok

    response = r.json()
    print(response)

    assert 'id' in response
    assert 'userType' in response
    assert 'userGroups' in response
    assert 'login' in response and response['login'] is not None
    assert 'fullPhoneNumber' in response
    assert 'email' in response
    assert 'firstName' in response
    assert 'lastName' in response
    assert 'patronymic' in response
    assert 'position' in response
    assert 'sex' in response
    assert 'birthDate' in response
    assert 'status' in response
    assert 'role' in response
    assert 'information' in response
    assert 'city' in response
    assert 'area' in response
    assert 'street' in response
    assert 'house' in response
    assert 'appartment' in response
    assert 'userSettings' in response
    assert 'registrationDate' in response

    for obj in response['userSettings']:
        assert 'name' in obj
        assert 'userId' in obj
        assert 'value' in obj

    # проверки ниже, до валидации схемы - опционально
    assert response['id'] == 8
    assert response['login'] == '79044444444'
    assert response['email'] == '790444444444@gmail.com'
    assert response['firstName'] == 'Евгений'
    assert response['lastName'] == 'Николаев'

    schema_path = './schemas'
    validate(response, json.load(
        open(f'{schema_path}/user_data.json', encoding='utf8')
    )
             )

    return response
